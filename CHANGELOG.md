# Changelog

All notable changes to this project will be documented in this file.

The format is based on Keep a Changelog, and this project adheres to Semantic Versioning.

## [3.0.4] - 26/11/2024
- changed `__construct(array $configuration, string $plugin_id, array $plugin_definition, ClientInterface $client, CacheBackendInterface $cache, FileSystemInterface $file_system)` to use less arguments and populate the client, cache and filesystem variables using the drupal service

## [3.0.3] - 26/08/2023
- drupal_get_path() and drupal_get_filename() have been deprecated in favor of extension listing services https://www.drupal.org/node/2940438 replaced all occurrences

## [3.0.2] - 03/02/2023
- Fix return value on `get` function.
- Replace `define` with `const`.

## [3.0.1] - 01/11/2022
- Implement support for `FeedsCustomSource` and remove `abstract` from `WebcastParser.php`.
- Bump `composer.json` `cern_indico_feeds` requirement to `^3.0.1`.

## [3.0.0] - 18/10/2022
- Update module to ensure PHP 8.1 compatibility
- Update HTTP links to HTTPS
- Remove `composer.json`
- Remove old `.bak` file
- Add `.gitignore`

## [2.0.5] - 03/12/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.0.4] - 09/02/2021

- Add core: 8.x to cern_webcast_events

## [2.0.3] - 09/02/2021

- Add feeds requirement in composer.json
- Remove deprecations for D9 (passes d9-readiness scan)

## [2.0.2] - 08/02/2021

- Add core: 8.x to fix enabling issue

## [2.0.1] - 15/01/2021

- Add composer.json file
- Update module to be D9-ready
- Move files one level higher
